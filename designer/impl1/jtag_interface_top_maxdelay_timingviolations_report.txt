Timing Violation Report Max Delay Analysis

SmartTime Version v11.9 SP5
Microsemi Corporation - Microsemi Libero Software Release v11.9 SP5 (Version 11.9.5.5)
Date: Thu Apr 30 11:32:30 2020


Design: jtag_interface_top
Family: ProASIC3
Die: A3P250
Package: 144 FBGA
Temperature Range: -40 - 85 C
Voltage Range: 1.425 - 1.575 V
Speed Grade: -1
Design State: Post-Layout
Data source: Silicon verified
Min Operating Conditions: BEST - 1.575 V - -40 C
Max Operating Conditions: WORST - 1.425 V - 85 C
Using Enhanced Min Delay Analysis
Scenario for Timing Analysis: Primary


No Path

