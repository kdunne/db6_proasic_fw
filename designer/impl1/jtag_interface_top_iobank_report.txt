********************************************************************
                            I/O Bank Report
********************************************************************
  
Product: Designer
Release: v11.9 SP5
Version: 11.9.5.5
Date: Thu Apr 30 11:32:09 2020
Design Name: jtag_interface_top  Family: ProASIC3  Die: A3P250  Package: 144 FBGA


I/O Function:

    Type                                  | w/o register  | w/ register  | w/ DDR register
    --------------------------------------|---------------|--------------|----------------
    Input I/O                             | 4             | 0            | 0
    Output I/O                            | 8             | 0            | 0
    Bidirectional I/O                     | 0             | 0            | 0
    Differential Input I/O Pairs          | 11            | 0            | 0
    Differential Output I/O Pairs         | 0             | 0            | 0

I/O Technology:

                                    |   Voltages    |             I/Os
    --------------------------------|-------|-------|-------|--------|--------------
    I/O Standard(s)                 | Vcci  | Vref  | Input | Output | Bidirectional
    --------------------------------|-------|-------|-------|--------|--------------
    LVTTL                           | 3.30v | N/A   | 4     | 8      | 0
    LVDS                            | 2.50v | N/A   | 22    | 0      | 0

I/O Bank Resource Usage:

          |   Voltages    | Single I/Os  | Diff I/O Pairs |        Vref I/Os
          |-------|-------|------|-------|-------|--------|------|-------|----------
          | Vcci  | Vref  | Used | Total | Used  | Total  | Used | Total | Vref Pins
    ------|-------|-------|------|-------|-------|--------|------|-------|----------
    Bank0 | 3.30v | N/A   | 4    | 24    | 0     | 0      | N/A  | N/A   | N/A
    Bank1 | 2.50v | N/A   | 0    | 25    | 11    | 12     | N/A  | N/A   | N/A
    Bank2 | 3.30v | N/A   | 8    | 23    | 0     | 0      | N/A  | N/A   | N/A
    Bank3 | 2.50v | N/A   | 0    | 25    | 0     | 12     | N/A  | N/A   | N/A

I/O Voltage Usage:

       Voltages    |     I/Os
    -------|-------|------|-------
     Vcci  | Vref  | Used | Total
    -------|-------|------|-------
     2.50v | N/A   | 22   | 50
     3.30v | N/A   | 12   | 47

