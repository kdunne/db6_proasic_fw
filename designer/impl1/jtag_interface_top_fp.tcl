new_project \
    -name {jtag_interface_top} \
    -location {\\vmware-host\Shared Folders\Documents\PostDoc\TileCal\proasic3\new_jtag_interface\designer\impl1\jtag_interface_top_fp} \
    -mode {single}
set_programming_file -file {\\vmware-host\Shared Folders\Documents\PostDoc\TileCal\proasic3\new_jtag_interface\designer\impl1\jtag_interface_top.pdb}
set_programming_action -action {PROGRAM}
run_selected_actions
save_project
close_project
