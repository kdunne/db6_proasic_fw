--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: jtag_interface_top.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::ProASIC3> <Die::A3P250> <Package::144 FBGA>
-- Author: <Name>
--
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;

library proasic3e; 
use proasic3e.all;

entity jtag_interface_top is
port (
	gbt_tms_p : IN  std_logic_vector(1 downto 0); -- 0 = local, 1 = remote
	gbt_tdi_p : IN  std_logic_vector(1 downto 0); 
	gbt_tck_p : IN  std_logic_vector(1 downto 0); 
	gbt_rst_p : IN  std_logic_vector(1 downto 0); 
	gbt_tms_n : IN  std_logic_vector(1 downto 0); -- 0 = local, 1 = remote
	gbt_tdi_n : IN  std_logic_vector(1 downto 0); 
	gbt_tck_n : IN  std_logic_vector(1 downto 0); 
	gbt_rst_n : IN  std_logic_vector(1 downto 0); 
	proasic_tms : OUT  std_logic_vector(1 downto 0); -- 0 = local, 1 = remote
	proasic_tdi : OUT  std_logic_vector(1 downto 0); 
	proasic_tck : OUT  std_logic_vector(1 downto 0); 
	proasic_rst : OUT  std_logic_vector(1 downto 0); 
    gbt_rxrdy       : IN   std_logic;
    gbt_dvalid      : IN   std_logic;
    gbt_key_a_p     : IN   std_logic;
    gbt_key_a_n     : IN   std_logic;
    gbt_key_b_p     : IN   std_logic;
    gbt_key_b_n     : IN   std_logic;
    gbt_clk_sig_p   : IN   std_logic;
    gbt_clk_sig_n   : IN   std_logic;
    gbt_clk40       : IN   std_logic;
    osc_clk40       : IN   std_logic);
end jtag_interface_top;
architecture arch_1 of jtag_interface_top is

   component CLKBUF 
    port (pad: in std_logic; y: out std_logic);
   end component;

    component JTAG_DIFF_INPUT is
    port( PADP : in    std_logic_vector(3 downto 0);
          PADN : in    std_logic_vector(3 downto 0);
          Y    : out   std_logic_vector(3 downto 0)
        );
    end component;

  component INBUF_LVDS
    port( PADP : in    std_logic := 'U';
          PADN : in    std_logic := 'U';
          Y    : out   std_logic
        );
  end component;


    component CLOCK_MANAGEMENT is

    port( POWERDOWN : in    std_logic;
          CLKA      : in    std_logic;
          LOCK      : out   std_logic;
          GLA       : out   std_logic;
          GLB       : out   std_logic
        );

end component;

procedure tmr (
        signal clk : in std_logic;
        signal inputs : in std_logic_vector(2 downto 0);
        signal sig : out std_logic;
        signal err : out std_logic ) is
  
    begin
        if rising_edge (clk) then
        case inputs is
            when "111" => 
                sig <= '1';
                err <= '0';
            when "000" => 
                sig <= '0';
                err <= '0';
            when others =>
                err <= '1';
                if (inputs = "011" or inputs = "101" or inputs = "110") then
                    sig <='1';
                else
                    sig <='0';
                end if;
        end case;
        end if; -- clock edge
end tmr;

component key_check is 
port(
    signal keya : in std_logic;
    signal keyb : in std_logic;
    signal clk   : in std_logic;
    signal rxready : in std_logic;
    signal ck_ok : in std_logic;
    signal key_ok : out std_logic );
end component;
 

component clock_freq_check is
port(
    signal clk40 : in std_logic;
    signal clk240 : in std_logic;
    signal rxready : in std_logic;
    signal ck_ok : out std_logic );


end component;

component clock_freq_check_tmr is
port(
    signal clk40 : in std_logic;
    signal clk240 : in std_logic;
    signal rxready : in std_logic;
    signal ck_ok : out std_logic );


end component;


	signal jtag_in_loc_p, jtag_in_loc_n: std_logic_vector(3 downto 0) ; 
	signal jtag_in_rem_p, jtag_in_rem_n: std_logic_vector(3 downto 0) ; 
	signal jtag_loc, jtag_rem: std_logic_vector(3 downto 0) ; 
	signal jtag_out_loc, jtag_out_rem: std_logic_vector(3 downto 0) ; 
    signal gbt_clk_sig, key_a, key_b, osc_clk40_buf, clk40, clk240: std_logic;
    signal jtag_loc_en, jtag_rem_en: std_logic;

    signal clock_ok_tmr, key_ok_tmr : std_logic_vector (2 downto 0) := "000";
    signal clock_ok, key_ok : std_logic := '0';
    signal clock_err, key_err : std_logic := '0';

    constant pdown : std_logic := '1';

begin

    proasic_tms(0) <= jtag_out_loc(0);
    proasic_tdi(0) <= jtag_out_loc(1);
    proasic_tck(0) <= jtag_out_loc(2);
    proasic_rst(0) <= jtag_out_loc(3);

    proasic_tms(1) <= jtag_out_rem(0);
    proasic_tdi(1) <= jtag_out_rem(1);
    proasic_tck(1) <= jtag_out_rem(2);
    proasic_rst(1) <= jtag_out_rem(3);


    jtag_in_loc_p <= (
            0 => gbt_tms_p(0),
            1 => gbt_tdi_p(0),
            2 => gbt_tck_p(0),
            3 => gbt_rst_p(0));

    jtag_in_loc_n <= (
            0 => gbt_tms_n(0),
            1 => gbt_tdi_n(0),
            2 => gbt_tck_n(0),
            3 => gbt_rst_n(0));

    jtag_in_rem_p <= (
            0 => gbt_tms_p(1),
            1 => gbt_tdi_p(1),
            2 => gbt_tck_p(1),
            3 => gbt_rst_p(1));

    jtag_in_rem_n <= (
            0 => gbt_tms_n(1),
            1 => gbt_tdi_n(1),
            2 => gbt_tck_n(1),
            3 => gbt_rst_n(1));


    jtag_in0: JTAG_DIFF_INPUT
        port map (jtag_in_loc_p, jtag_in_loc_n, jtag_loc);

    jtag_in1: JTAG_DIFF_INPUT
        port map (jtag_in_rem_p, jtag_in_rem_n, jtag_rem);

    keya: INBUF_LVDS
        port map (gbt_key_a_p, gbt_key_a_n, key_a);

    keyb: INBUF_LVDS
        port map (gbt_key_b_p, gbt_key_b_n, key_b);

    clksig: INBUF_LVDS
        port map (gbt_clk_sig_p, gbt_clk_sig_n, gbt_clk_sig);

    -- key check (TMR)
    kc1:key_check port map(key_a, key_b, gbt_clk_sig, gbt_rxrdy, clock_ok, key_ok_tmr(0));
    kc2:key_check port map(key_a, key_b, gbt_clk_sig, gbt_rxrdy, clock_ok, key_ok_tmr(1));
    kc3:key_check port map(key_a, key_b, gbt_clk_sig, gbt_rxrdy, clock_ok, key_ok_tmr(2));
    tmr (gbt_clk40, key_ok_tmr, key_ok, key_err);

    -- clock check (TMR)
    fc0:clock_freq_check port map(clk40 => gbt_clk40, clk240 => clk240, rxready => gbt_rxrdy, ck_ok => clock_ok_tmr(0));
    fc1:clock_freq_check port map(clk40 => gbt_clk40, clk240 => clk240, rxready => gbt_rxrdy, ck_ok => clock_ok_tmr(1));
    fc2:clock_freq_check port map(clk40 => gbt_clk40, clk240 => clk240, rxready => gbt_rxrdy, ck_ok => clock_ok_tmr(2));
    tmr (gbt_clk40, clock_ok_tmr, clock_ok, clock_err);
--    fctmr: clock_freq_check_tmr port map(clk40 => gbt_clk40, clk240 => clk240, rxready => gbt_rxrdy, ck_ok => clock_ok);


--    osc_buffer : CLKBUF
--        port map (osc_clk40, osc_clk40_buf);

    osc_clk40_buf <= osc_clk40;

    clocking: CLOCK_MANAGEMENT
        port map (POWERDOWN => pdown, CLKA => osc_clk40_buf, LOCK => open, GLA => clk40, GLB => clk240);


    jtag_loc_en <= gbt_dvalid and key_ok and clock_ok;
    jtag_rem_en <= gbt_dvalid and key_ok and clock_ok;

    enable_outputs: process(clk40, jtag_loc_en, jtag_rem_en)
       begin
        if falling_edge (clk40) then
            if jtag_loc_en then
                jtag_out_loc <= jtag_loc;
            else
                jtag_out_loc <= (others => 'Z');
            end if;

            if jtag_rem_en then
                jtag_out_rem <= jtag_rem;
            else
                jtag_out_rem <= (others => 'Z');
            end if;
       end if;
    end process;
end arch_1;
