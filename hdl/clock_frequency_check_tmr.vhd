--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: clock_frequency_check_tmr.vhd
--------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;

library synplify;

entity clock_freq_check is
port (
    signal clk40 : in std_logic;
    signal clk240 : in std_logic;
    signal rxready : in std_logic;
    signal ck_ok : out std_logic
);
end clock_freq_check;
architecture arch1 of clock_freq_check is

attribute syn_radhardlevel : string;
attribute syn_radhardlevel of arch1:architecture is "tmr";
 
    signal final_count : integer range 0 to 15 := 0;

begin

proc1: process(clk240)
     variable state : std_logic := '0';
    variable counter: integer range 0 to 15 := 0;

    begin

    if rising_edge(clk240) then
        counter := counter + 1;
        case state is
            when '0' =>
                if (clk40 = '1') then
                    state := '1';
                end if;
            when '1' =>
                if (clk40 = '0') then
                    final_count <= counter;
                    state := '0';
                    counter := 0;
                end if;
            end case;

            if (rxready = '1' and (final_count < 5 or final_count > 6)) then
                ck_ok <= '0';
            else
                ck_ok <= '1';
            end if;

    end if; -- clock edge


end process;

end arch1;
