--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: clock_freq_check.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::ProASIC3> <Die::A3P250> <Package::144 FBGA>
-- Author: <Name>
--
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;

entity clock_freq_check is
port (
    signal clk40 : in std_logic;
    signal clk240 : in std_logic;
    signal rxready : in std_logic;
    signal ck_ok : out std_logic
);
end clock_freq_check;
architecture arch1 of clock_freq_check is
 
    signal final_count : integer range 0 to 15 := 0;

begin

proc1: process(clk240)
     variable state : std_logic := '0';
    variable counter: integer range 0 to 15 := 0;

    begin

    if rising_edge(clk240) then
        counter := counter + 1;
        case state is
            when '0' =>
                if (clk40 = '1') then
                    state := '1';
                end if;
            when '1' =>
                if (clk40 = '0') then
                    final_count <= counter;
                    state := '0';
                    counter := 0;
                end if;
            end case;

            if (rxready = '1' and (final_count < 5 or final_count > 6)) then
                ck_ok <= '0';
            else
                ck_ok <= '1';
            end if;

    end if; -- clock edge


end process;

end arch1;
